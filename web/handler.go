package web

import (
	"encoding/json"
	"net/http"
)

func getHome(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode("hello world")
}
