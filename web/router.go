package web

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func Run(port int) {
	var b strings.Builder
	b.WriteString(":")
	b.WriteString(strconv.Itoa(port))

	router := mux.NewRouter()

	router.HandleFunc("/", getHome).Methods("GET")

	log.Fatal(http.ListenAndServe(b.String(), router))
}
