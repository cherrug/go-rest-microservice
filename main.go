package main

import (
	"gitlab.com/cherrug/go-rest-microservice/cfg"
	"gitlab.com/cherrug/go-rest-microservice/web"
)

func main() {
	c := cfg.Read()
	web.Run(c.Port)
}
