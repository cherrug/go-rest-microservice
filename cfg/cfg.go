package cfg

import (
	"github.com/spf13/viper"
	"log"
)

type Cfg struct {
	Web
	Logger
}

type Web struct {
	Port int
	Host string
}

type Logger struct {
	File  string
	Level string
}

func Read() Cfg {
	var c Cfg

	v := viper.New()

	v.SetConfigName("cfg")
	v.AddConfigPath(".")
	v.AddConfigPath("./cfg")
	v.AutomaticEnv()
	err := v.ReadInConfig()
	if err != nil {
		log.Fatalf("failed to read config, %v", err)
	}

	err = v.Unmarshal(&c)
	if err != nil {
		log.Fatalf("failed to unmarhal config, %v", err)
	}

	return c

}
