module gitlab.com/cherrug/go-rest-microservice

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20190508220229-2d0786266e9c // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
)
